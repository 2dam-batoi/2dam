from odoo import models, fields, api


class libreria_categoria(models.Model):
    _name = 'libreria.categoria'
    # _description = 'libreria.libreria'
    name = fields.Char(string="Nombre", required=True, help="LLorens Mompo")
    description = fields.Text(string="Descripión")
    libro = fields.One2many("libreria.libro","categoria",string="Libros")


class libreria_libro(models.Model):
    _name = 'libreria.libro'
    # _description = 'libreria.libreria'
    name = fields.Char(string="Título", required=True, help="Introduce el nombre del libro")
    precio = fields.Float(string="Precio")
    ejemplares = fields.Integer(string="Ejemplares")
    fecha = fields.Date(string="Fecha de compra")
    segmano = fields.Boolean(string="Segunda mano")
    estado = fields.Selection([('0', 'Bueno'), ('1', 'Regular'), ('2', 'Malo')], string="Estado del libro", default="0")
    categoria = fields.Many2one("libreria.categoria",string="Categoría",required=True,ondelete="cascade")
